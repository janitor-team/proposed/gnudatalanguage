#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

CXXFLAGS += -DBUILD_DATE="\"$(shell LC_ALL=C date -u --date=@"$(SOURCE_DATE_EPOCH)" +'%b %e %Y')\"" -std=gnu++11

# Lift the usual restrictions on the size of the global offset table.
# Taken from the gcc manpage
# earlier taken from https://bugs.debian.org/895214 (less debug info
# to avoid running out of address space)
ifneq (,$(filter $(DEB_HOST_ARCH), mips mipsel))
  export CXXFLAGS += -mxgot
endif

# Workaround for
# https://github.com/gnudatalanguage/gdl/issues/677
LDFLAGS += -Wl,--allow-multiple-definition

%:
	dh $@ -Scmake --with python3

override_dh_auto_configure:
	dh_auto_configure -- \
	  -DDEB_BUILD_MULTIARCH=`dpkg-architecture -qDEB_BUILD_MULTIARCH` \
	  -DHDF5=NO \
	  -DFFTW=YES \
	  -DLIBPROJ4=YES \
	  -DGLPK=YES \
	  -DPYTHON=YES \
	  -DPYTHONVERSION=3 \
	  -DPYTHON_MODULE=YES \
	  -DWXWIDGETS=YES \
	  -DGRIB=YES \
	  -DQHULL=YES \
	  -DUDUNITS=YES \
	  -DPSLIB=YES

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	( cd obj*/testsuite \
	&& $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o libtest_ce.so ../../testsuite/libtest_ce.cpp -shared -fPIC )
	LD_LIBRARY_PATH="../src" xvfb-run dh_auto_test --no-parallel -Scmake || true
endif


override_dh_compress:
	dh_compress -X.pro

override_dh_auto_install:
	dh_auto_install
	dh_numpy3
