Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: GDL - GNU Data Language
Upstream-Contact: gnudatalanguage-devel@lists.sourceforge.net
                  Sylwester Arabas <sarabas@igf.fuw.edu.pl>
                  Alain Coulais <alain.coulais@obspm.fr>
Source: http://gnudatalanguage.sourceforge.net/downloads.php
Files-Excluded: resource/maps/low/*.html resource/maps/high/*.html

Files: *
Copyright: 2001 Deepak Bandyopadhyay
 2001-2007, J. D. Smith
 2001 Lutz Kettner
 2002-2012 Joel Gales <jomoga@users.sourceforge.net>
 2002-2013 Marc Schellens <m_schellens@users.sourceforge.net>
 2004 Christopher Lee <c_lee@users.sourceforge.net>
 2004 Peter Messmer <messmer@users.sourceforge.net>
 2004-2006 Pierre Chanial <pchanial@users.sourceforge.net>
 2005 SJT
 2006-2020 Alain Coulais <alaingdl@users.sourceforge.net>
 2007 Gregory Marchal <gregory.marchal@gmail.com>
 2007 Sebastien Masson
 2008 Nicolas Galmiche
 2008-2011 Sylwester Arabas
 2008-2013 Philippe Prugniel
 2009 John Serafin
 2010 Rene Preusker
 2010 Lea Noreskal
 2010-2012 Maxime Lenoir
 2010 Orion Poplawski
 2011 Mateusz Turcza <mturcza@mimuw.edu.pl>
 2011 Pedro Corona
 2011 Hong Xu
 2012-2014 Jeongbin Park
 2013-2020 Gilles Duvert <gilles-duvert@users.sourceforge.net>
 2013 Tim
 2013 Sacha Hony
 2014 Levan Loria
 2015 NATCHKEBIA Ilia
 2018 Remi A. Solås
 1996-2014 P. Wessel, W. H. F. Smith
License: GPL-2+

Files: src/pro/value_locate.pro
Copyright: 2006 Richard Schwartz
 2000-2001 Craig Markwardt
License: 4-line-bsdish

Files: src/pro/dicom/*
Copyright: 2004-2006 Robbie Barnett
License: LGPL-2.1+

Files: resource/maps/*
Copyright: not applicable
License: public-domain
 As facts, these files are put into the public domain by Natural Earth.

Files: resource/maps/low/continents.* resource/maps/high/continents_med.*
Copyright: 2017 Paul Wessel, SOEST, University of Hawai'i, Honolulu, HI.
 Walter H. F. Smith, NOAA Geosciences Lab, National Ocean Service, Silver Spring, MD.
License: LGPL-3+

Files: src/antlr/*
       src/print_tree.*
       CMakeModules/FindImageMagick.cmake
       src/dSFMT/*
Copyright: 2003-2007 Terence Parr
 2007-2009 Kitware, Inc.
 2007-2008 Miguel A. Figueroa-Villanueva <miguelf at ieee dot org>
 2012 Rolf Eike Beer <eike@sf-mail.de>
 2013 Nodar Kasradze
 2002-2011 Mutsuo Saito, Makoto Matsumoto, Hiroshima University
License: BSD-3-Clause

Files: debian/*
Copyright: 2006 Sergio Gelato <Sergio.Gelato@astro.su.se>
           2007 Juan A. Añel <atherlux@gulo.org>
           2007-2011 Gürkan Sengün <tar@debian.org>
           2011-2014 Axel Beckert <abe@debian.org>
    2017-2018 Ole Streicher <olebole@debian.org>
License: GPL-2+

Files: CMakeModules/FindEigen3.cmake
Copyright: 2006-2007 Montel Laurent <montel@kde.org>
           2008-2009 Gael Guennebaud <g.gael@free.fr>
           2009      Benoit Jacob <jacob.benoit.1@gmail.com>
License: BSD-2-Clause

Files: src/base64.hpp
Copyright: 1999 Bob Withers <bwit@pobox.com>
License: 3-line-bsdish

Files: src/basic_pro.cpp
       src/file.cpp
Copyright: 1997-2012 Kevlin Henney
           2002-2004 Marc Schellens
License: GPL-2+ and 5-line-bsdish

Files: src/pro/envi/*
       src/pro/hist_equal.pro
       src/pro/standardize.pro
Copyright: 2010-2012 Josh Sixsmith
License: GPL-3+

Files: src/randomgenerators.cpp
       src/gsl_fun.cpp
Copyright: 2004      Joel Gales
    1996-2007 James Theiler, Brian Gough
           2006      Charles Karney
License: GPL-2+ and GPL-3+

Files: src/pro/chisqr_cvf.pro src/medianfilter.cpp
Copyright: 2017 Dominic H.F.M. Schnitzeler
 2006  Simon Perreault
 2014 Jukka Suomela
 2011 ashelly.myopenid.com
License: Expat

License: 3-line-bsdish
 This code may be freely used for any purpose, either personal or
 commercial, provided the authors copyright notice remains intact.
Comment: The original formatting was three lines.

License: 4-line-bsdish
 This software is provided as is without any warranty whatsoever.
 Permission to use, copy, modify, and distribute modified or
 unmodified copies is granted, provided this copyright and disclaimer
 are included unchanged.

License: 5-line-bsdish
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose is hereby granted without fee, provided
 that this copyright and permissions notice appear in all copies and
 derivatives.
 .
 This software is supplied "as is" without express or implied
 warranty.
Comment: The original formatting was five lines, not counting blank
 lines.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1) Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2) Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the
   distribution.
 .
 * Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at
 your option) any later version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU Lesser
 General Public License 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3+
 On Debian GNU/Linux systems, the complete text of the GNU Lesser
 General Public License 3 can be found in
 `/usr/share/common-licenses/LGPL-3'.
